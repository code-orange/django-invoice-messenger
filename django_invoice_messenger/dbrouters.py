class InvoiceMessengerRouter:
    """
    A router to control all database operations on models in the
    django_invoice_messenger application.
    """

    def db_for_read(self, model, **hints):
        """
        Attempts to read django_invoice_messenger models go to auth_db.
        """
        if model._meta.app_label == "django_cks_dms_models":
            return "cks_dms"
        if model._meta.app_label == "django_sap_business_one_models":
            return "sbo_company"
        if model._meta.app_label.startswith("django_mdat_"):
            return "mdat"

        return None

    def db_for_write(self, model, **hints):
        """
        Attempts to write django_invoice_messenger models go to auth_db.
        """
        if model._meta.app_label == "django_cks_dms_models":
            return "cks_dms"
        if model._meta.app_label == "django_sap_business_one_models":
            return "sbo_company"
        if model._meta.app_label.startswith("django_mdat_"):
            return "mdat"

        return None

    def allow_relation(self, obj1, obj2, **hints):
        return None

    def allow_migrate(self, db, app_label, model_name=None, **hints):
        if db == "mdat":
            return False

        return None
